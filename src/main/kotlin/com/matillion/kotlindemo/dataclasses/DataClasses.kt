package com.matillion.kotlindemo.dataclasses

import com.matillion.kotlindemo.trailinglambdas.section

data class Table(
        val database: String,
        val schema: String,
        val name: String
)

fun main() {
    val table1 = Table("my_database", "my_schema", "my_table")
    val table2 = Table("my_database", "my_schema", "my_table")

    section("Data class build-in methods") {

        // Provides hashcode, equals and toString implementations
        println("${table1.hashCode()} vs ${table2.hashCode()}")
        println(table1)

    }

    section("== vs ===") {

        // == is .equals()
        println(table1 == table2)
        println(table1 === table2)

    }

    section("Object destructuring") {

        // Object destructuring
        val (db, _, name) = table1
        println(db)
        println(name)

    }
}