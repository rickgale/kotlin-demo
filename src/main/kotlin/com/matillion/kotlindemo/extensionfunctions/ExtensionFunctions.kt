package com.matillion.kotlindemo.extensionfunctions

import com.matillion.kotlindemo.trailinglambdas.section

// Extension functions (and methods with expression bodies)
fun String.evenHash(): Boolean = hashCode() % 2 == 0

fun main() {

    section("Extension functions") {

        println("Test".evenHash())
        println("Tests".evenHash())

    }

}