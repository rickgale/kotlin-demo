package com.matillion.kotlindemo.operatoroverloading

import com.matillion.kotlindemo.trailinglambdas.section

operator fun String.div(count: Int): String {
    return this.takeLast(count)
}

fun main() {

    section("Operator overloading") {

        println("I like cookies" / 7)

    }
}