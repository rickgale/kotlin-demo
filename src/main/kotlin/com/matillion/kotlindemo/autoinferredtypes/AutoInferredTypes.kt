package com.matillion.kotlindemo.autoinferredtypes

import com.matillion.kotlindemo.trailinglambdas.section

// Method with expression body
fun getSomething() = "Test".hashCode()

// But be careful with expression bodies
fun thisMightDoSomething() = {
    "Hopefully this does something".length
}

fun main() {
    section("Auto inferred types") {

        val myName = "Rick"

        println(myName.length)
        println(myName.hashCode())

    }

    section("Expression bodies") {

        println(getSomething())

        println(thisMightDoSomething())
        println(thisMightDoSomething()())

    }

    section("Val vs Var") {
        var aNumber = 10
        println(aNumber)

        aNumber = 20
        println(aNumber)
    }
}