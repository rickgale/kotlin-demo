package com.matillion.kotlindemo.javainterop

import com.matillion.kotlindemo.trailinglambdas.section

class Person(
        val name: String,
        var age: Int
) {

    fun announce() {
        println("Hey my name is $name and I'm $age")
    }

}

fun main() {

    section("Calling Java from Kotlin (and prroperties become getters/setters)") {

        PersonInterop().doSomething()

    }

    section("Calling Kotlin from Java") {

        PersonInterop().doAnnounce()

    }

    section("Getters/setters become properties") {

        val car = Car("Ford ", "GT", 10.0)
        println(car.make)
        println(car.speed)
        car.speed = 20.0
        println(car.speed)

    }
}