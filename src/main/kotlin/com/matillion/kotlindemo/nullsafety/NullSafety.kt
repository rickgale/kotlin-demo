package com.matillion.kotlindemo.nullsafety

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.matillion.kotlindemo.trailinglambdas.section

class Aircraft(
        val name: String,
        val radar: Radar? = null
)

class Radar(
        val name: String,
        val emitter: Emitter?
)

class Emitter(
        val name: String,
        val power: Double?
)

fun main() {

    section("Null chaining") {

        val aircraft = ObjectMapper()
            .registerKotlinModule()
            .readValue<Aircraft>(Aircraft::class.java.getResource("/aircraft.json").readText())

        println(aircraft.radar?.emitter?.power ?: -1)

    }

    section("Default arguments") {
        
        println(Aircraft("Empty aircraft").radar?.emitter?.power ?: -1)

    }

}