package com.matillion.kotlindemo.trailinglambdas

fun section(
    name: String,
    block: () -> Unit
) {
    println("---------- $name ----------")
    block()
    println("---------- /$name ---------")
    println()
}