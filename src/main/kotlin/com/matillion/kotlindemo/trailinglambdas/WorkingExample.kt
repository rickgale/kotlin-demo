package com.matillion.kotlindemo.trailinglambdas

import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.or
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import org.slf4j.LoggerFactory
import java.util.*

object Culprit : Table("ci_culprits") {

    val id = long("id")
    val name = varchar("name", 255)
    val url = varchar("url", 255)

}

object WorkingExample {

    private val LOGGER = LoggerFactory.getLogger(javaClass)

    fun run() {
        connectToDb()

        transaction {

            Culprit
                .select {
                    Culprit.name.like("%Rick%") or Culprit.name.like("%Ben%") or Culprit.url.like("%chris%")
                }
                .sortedBy { it[Culprit.name].toLowerCase() }
                .forEach {
                    LOGGER.info("Found culprit: ${it[Culprit.name]}")
                }

        }
    }

    fun connectToDb() {
        val datasourceProps = Properties().apply {
            try {
                WorkingExample::class.java.getResource("/datasource.properties")
                    .openStream()
                    .use {
                        load(it)
                    }
            } catch (exc: NullPointerException) {
                LOGGER.error("IO exception loading properties - have you setup the datasource properties file?")
                throw exc
            }
        }

        Database.connect(
            datasourceProps.getProperty("URL"),
            driver = "org.postgresql.Driver",
            user = datasourceProps.getProperty("USERNAME"),
            password = datasourceProps.getProperty("PASSWORD")
        )
    }

}

fun main() {
    WorkingExample.run()
}