package com.matillion.kotlindemo.expressions

import com.matillion.kotlindemo.trailinglambdas.section

fun main() {
    section("When expressions") {

        val filmNumber = 2
        val result = when (filmNumber) {
            1 -> "The fellowship"
            2 -> "The two towers"
            3 -> "The return of the king"
            // (Yet to be released)
            4 -> "Murder in Mordor"
            5 -> "Getting rowdy in Rohan"
            else -> null
        }

        println(result)

    }

    section("Try/catch expressions") {

        val result = try {
            "Please work"
        } catch (e: Exception) {
            "Oh noes it didn't work (because ${e.message})"
        }

        println(result)

    }

    section("If expressions") {

        val result = if ("Dogs".hashCode() > "Cats".hashCode()) {
            "Dogs are better than cats"
        } else {
            "Cats are better than dogs"
        }

        println(result)

    }
}