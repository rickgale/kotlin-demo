package com.matillion.kotlindemo.javainterop;

class Car {

    private final String make;
    private final String model;
    private double speed;

    public Car(String make, String model, double speed)
    {
        this.make = make;
        this.model = model;
        this.speed = speed;
    }

    public String getMake()
    {
        return make;
    }

    public String getModel()
    {
        return model;
    }

    public double getSpeed()
    {
        return speed;
    }

    public void setSpeed(double speed)
    {
        this.speed = speed;
    }
}