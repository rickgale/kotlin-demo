package com.matillion.kotlindemo.javainterop;

public class PersonInterop
{

    private final Person person = new Person("Rick", 30);

    public void doSomething() {
        System.out.println(person.getName());

        System.out.println(person.getAge());
        person.setAge(10);
        System.out.println(person.getAge());
    }

    public void doAnnounce() {
        person.announce();
    }

}
