import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

// Plugin config
plugins {
	id("org.springframework.boot") version "2.3.2.RELEASE"
	id("io.spring.dependency-management") version "1.0.9.RELEASE"
	kotlin("jvm") version "1.4.10"
}

// App metadata
group = "com.matillion"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

configurations {
	compileOnly {
		extendsFrom(configurations.annotationProcessor.get())
	}
}

// Setup repos
repositories {
	mavenCentral()
	jcenter()
}

// Deps
dependencies {

	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.9.8")

	implementation("ch.qos.logback:logback-core:1.2.3")
	implementation("ch.qos.logback:logback-classic:1.2.3")
	implementation("org.slf4j:slf4j-api:1.7.30")

	implementation("org.jetbrains.exposed:exposed-core:0.28.1")
	implementation("org.jetbrains.exposed:exposed-dao:0.28.1")
	implementation("org.jetbrains.exposed:exposed-jdbc:0.28.1")

	implementation("org.postgresql:postgresql:42.2.18")

	testImplementation("org.junit.jupiter:junit-jupiter:5.6.2")
	testImplementation("org.hamcrest:hamcrest:2.2")
	testImplementation("io.mockk:mockk:1.10.0")

}

tasks.withType<Test> {
	useJUnitPlatform()
}

// Setup kotlin
tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}
